FROM jenkins_slave
LABEL maintainer="flutters@fluttershub.xyz"

USER root
RUN yum install -y docker
USER jenkins
ADD dockerconfig /etc/default/docker